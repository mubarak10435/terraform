# Configure the AWS Provider
provider "aws" {
  version = "~> 4.0"
  region  = var.region
}
resource "aws_security_group" "app-sg" {
  name = var.mot_sg
  vpc_id = var.vpc_id

  // To Allow SSH Transport
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  // To Allow Port 80 Transport
  ingress {
    from_port = 8080
    protocol = "tcp"
    to_port = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }

  // To Allow Port 80 Transport
  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

}

resource "aws_instance" "app" {
  ami = var.ami
  instance_type = var.itype
  subnet_id = var.subnet
  associate_public_ip_address = var.public_enable
  key_name = "mkey"


  vpc_security_group_ids = [
    aws_security_group.app-sg.id
  ]
  root_block_device {
    delete_on_termination = true
    volume_size = 10
    volume_type = "gp2"
  }
  tags = {
    Name ="mubarak"
    Environment = "DEV"


  }

  depends_on = [ aws_security_group.app-sg ]
}
