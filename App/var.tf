variable "region" {
default = "us-east-1"
}
variable "mot_sg" {
default = "mk"
}
variable "vpc_id" {
default = "vpc-0806a71f1547756ed"
}
variable "ami" {
default = "ami-06e46074ae430fba6"
}
variable "itype" {
default = "t2.micro"
}
variable "subnet" {
default = "subnet-06c3b89f607665d99"
}
variable "public_enable" {
default = "true"
}
