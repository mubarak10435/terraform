# Configure the AWS Provider
provider "aws" {
  version = "~> 4.0"
  region  = var.region
}

resource "aws_vpc" "mot" {
  cidr_block = var.vpc_cidr

  tags = {
    Name        = var.vpc_name

  }
}


resource "aws_subnet" "mot" {
  vpc_id     = aws_vpc.mot.id
  cidr_block = var.mot_cidr

  tags = {
    Name = "Mot-public"
  }
}
resource "aws_subnet" "mubarak" {
  vpc_id     = aws_vpc.mot.id
  cidr_block = var.mubarak_cidr

  tags = {
    Name = "mubarak-public"
  }
}
resource "aws_internet_gateway" "mot-igw" {
  vpc_id = aws_vpc.mot.id
  tags = {
    Name = "Mot-igw"
  }

}
resource "aws_route_table" "mot_route" {
  vpc_id = aws_vpc.mot.id


  tags = {
    Name = "mot-route-public"
  }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.mot-igw.id
  }

  route {
    ipv6_cidr_block        = "::/0"
    gateway_id = aws_internet_gateway.mot-igw.id
  }

}
